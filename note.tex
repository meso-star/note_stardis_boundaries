\documentclass[epsf,psfig,fancyheadings,12pt]{article}
%\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{graphics}
\usepackage{subfigure}
\usepackage[final]{epsfig}
\usepackage{mathrsfs}
%\usepackage{auto-pst-pdf}
%\usepackage{pstricks,pst-node,pst-text,pst-3d}
%\usepackage{mathbbm}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{rotating}
\usepackage{hyperref}
%\usepackage[3D]{movie15}
%\usepackage[]{endfloat}
\usepackage{animate}
\makeatletter
% double spacing lines 
\renewcommand{\baselinestretch}{1}
\newcommand{\LyX}{L\kern-.1667em\lower.25em\hbox{Y}\kern-.125emX\spacefactor1000}
%pour faire reference toujours avec la meme norme
%lyx !
\newcommand{\eq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\fig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\tab}[1]{Tab.~\ref{tab:#1}}
\newcommand{\para}[1]{Sec.~\ref{para:#1}}
\newcommand{\ap}[1]{Appendix~\ref{ap:#1}}
\newcommand{\ul}{\underline}
%
\makeatother

%\fancyhf{}%
% Utiliser \fancyhead[L] pour mettre du texte dans la partie de gauche
% du header
%\fancyhead[L]{}%
%\fancyhead[R]{\thepage}%
%\renewcommand{\headrulewidth}{0pt} % ça c'est pour faire une ligne horizontale
%\headsep=25pt % ça c'est pour laisser de la place entre le header et le texte
%\pagestyle{fancy}%

% margins
\setlength{\topmargin}{-.5in}
\setlength{\textheight}{9in}
\setlength{\oddsidemargin}{.125in}
\setlength{\textwidth}{6.25in}

\pdfsuppresswarningpagegroup=1


\begin{document}

\title{STARDIS: ce qu'il se passe en proche paroi pour la prise en compte des termes source}

\maketitle
\begin{center}
\author{V. Eymet, Méso-Star (\url{http://www.meso-star.com})} \\
\end{center}
\vspace{1cm}

\newpage
\tableofcontents

\newpage
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\section{Introduction}

Le code Stardis jusqu'à la version 0.14 souffre d'un problème de prise en compte des termes sources lors de la marche diffusive en proche interface. Le problème a été diagnostiqué, notamment, lors de l'élaboration de la solution analytique au problème représenté en figure \ref{fig:cavity}\footnote{Note technique Méso-Star et code de résolution, disponibles à partir du dépôt: \noindent\begin{minipage}{\linewidth}git@gitlab.com:meso-star/1d\_analytical\_unstationary.git\end{minipage}}: une coquille sphérique constituée d'un nombre arbitraire de matériaux (avec prise en compte possible d'une résistance thermique à l'interface entre deux matériaux successifs, et un possible terme source dans chaque matériau) contient un fluide interne de température inconnue. La figure \ref{fig:nullP_e20} permet de comparer les résultats obtenus par la méthode semi-analytique à ceux de Stardis dans le cas où tous le terme source de chaque solide est nul, pour la température du fluide interne, et au milieu d'un solide. Les résultats correspondent bien. Mais dès qu'un terme source doit être pris en compte dans les différents solides (figures \ref{fig:positiveP_e20} et \ref{fig:positiveP_e200}), les résultats divergent au fur et à mesure que le temps sonde s'éloigne de la condition initiale, et ce même si on réduit la valeur du pas de marche diffusive: une valeur de ce pas $\delta$ égale à l'épaisseur de chaque matériau divisée par 20 est utilisée dans la figure \ref{fig:positiveP_e20}, tandis que la valeur de $\delta$ pour la figure \ref{fig:positiveP_e200} est égale à l'épaisseur de chaque matériau divisée par 200. Cette différence de résultats pour l'état stationnaire est le symptôme d'une mauvaise prise en compte des termes sources lors du début et de la fin de la marche diffusive dans les solides (injection de la marche près des parois, et arrivée de la marche en paroi). Cette note vise à décrire la base sur laquelle la prise en compte de ces termes source a été repensée dans les versions de Stardis > 0.14

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{./figures/Cavity_N-materials.png}
\caption[]{Représentation graphique de la géométrie de test: une cavité sphérique de rayon $R_{1}$ détoure un volume de fluide de propriétés thermophysiques connues, mais de température inconnue $T_{F,1}(t)$; la coquille sphérique est constituée d'une suite d'un empilement de $N$ matériaux de propriétés thermophysiques connues, avec interfaces thermiques au contact entre les divers matériaux, et terme source pour chaque matériau. La surface externe échange par convection avec un fluide de propriétés et température connues, et par rayonnement avec une ambiance de température radiative connue. Un flux de ventilation peut être pris en compte. Les températures initiales de tous les fluides et solides sont connues.}
\label{fig:cavity}
\end{figure}

\begin{figure}[!htbp]
\hspace*{-1.5cm}
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/Tfluid_nullP_e20.pdf,width=0.6\textwidth}}\quad
\hspace*{-1.5cm}
     \subfigure[]{\epsfig{figure=figures/Tsolid_nullP_e20.pdf,width=0.6\textwidth}}}
  \caption[]{Evolution temporelle de la température (a) du fluide interne à la cavité et (b) au milieu du second solide (sur un total de 4) dans le cas où le terme source dans chaque solide est nul; les résultats analytiques sont représentés, ainsi que ceux obtenus par Stardis.}
\label{fig:nullP_e20}
\end{figure}

\begin{figure}[!htbp]
\hspace*{-1.5cm}
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/Tfluid_positiveP_e20.pdf,width=0.6\textwidth}}\quad
\hspace*{-1.5cm}
     \subfigure[]{\epsfig{figure=figures/Tsolid_positiveP_e20.pdf,width=0.6\textwidth}}}
  \caption[]{Evolution temporelle de la température (a) du fluide interne à la cavité et (b) au milieu du second solide (sur un total de 4) dans le cas où le terme source dans chaque solide est positif; les résultats analytiques sont représentés, ainsi que ceux obtenus par Stardis pour un $\delta$ de marche diffusive égal à l'épaisseur de chaque matériau divisé par 20.}
\label{fig:positiveP_e20}
\end{figure}

\begin{figure}[!htbp]
\hspace*{-1.5cm}
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/Tfluid_positiveP_e200.pdf,width=0.6\textwidth}}\quad
\hspace*{-1.5cm}
     \subfigure[]{\epsfig{figure=figures/Tsolid_positiveP_e200.pdf,width=0.6\textwidth}}}
  \caption[]{Evolution temporelle de la température (a) du fluide interne à la cavité et (b) au milieu du second solide (sur un total de 4) dans le cas où le terme source dans chaque solide est positif; les résultats analytiques sont représentés, ainsi que ceux obtenus par Stardis pour un $\delta$ de marche diffusive égal à l'épaisseur de chaque matériau divisé par 200.}
\label{fig:positiveP_e200}
\end{figure}

%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\section{Arrivée de la marche à une interface}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.7\textwidth]{./figures/walks_end.png}
\caption[]{Fin d'une marche conductive dans un solide de conductivité $\lambda$: les quatre dernières positions échantillonnées sont représentées, ainsi que la position en paroi lorsque cette dernière est détectée.}
\label{fig:walks_end}
\end{figure}

Le raisonnement va être conduit sur la base d'un solide semi-infini de conductivité $\lambda$ [$W.m^{-1}.K^{-1}$], muni d'un terme source $p$ [$W.m^{-3}$]. Le profil de température dans le solide est quadratique: $T(x)=-\frac{p}{2\lambda}x^{2}+ax+b$, la valeur des coefficients ($a$,$b$) dépendant du référentiel. Lors de la marche aléatoire de pas $\delta_{ref}$ (cf. figure \ref{fig:walks_end}), un terme d'accumulation est incrémenté de $\frac{p}{2D}\delta_{ref}^{2}$ avec $D$ la dimension du problème ($D$=2 out 3). En proche-paroi (position $\vec{x_{N}}$ de fin de trajet sur la figure \ref{fig:walks_end}), la paroi est détectée à une distance $\delta_{p} < \delta_{ref}$ de la position courante. On se pose alors la question de savoir de quelle quantité $\alpha$ augmenter l'accumulateur dans cette situation, indépendamment du fait que ce soit la température $T(\vec{x_{N}}+delta_{p}\vec{n})$ ou $T(\vec{x_{N}}-delta_{p}\vec{n})$. La température en $\vec{x_{N}}$ s'écrit alors, en régime stationnaire:

%-----------------------------------------------------------------------------------------------
\subsection{En 2D}

\begin{equation}
  \begin{split}
    T(\vec{x_{N}})=\int_{0}^{\pi/2} \frac{1}{\pi/2} &d\theta \Bigl\{\mathcal{H}\bigl(\delta_{ref}cos(\theta)<\delta_{p}\bigr)\Bigl[\frac{1}{2}T(\vec{x_{N}}+\delta_{ref}cos(\theta)\vec{n})+\frac{1}{2}T(\vec{x_{N}}-\delta_{ref}cos(\theta)\vec{n})+\frac{p}{4}\delta_{ref}^{2}\Bigr]\\
    &+\mathcal{H}\bigl(\delta_{ref}cos(\theta)>\delta_{p}\bigr)\Bigl[\frac{1}{2}T(\vec{x_{N}}+\delta_{p}\vec{n})+\frac{1}{2}T(\vec{x_{N}}-\delta_{p}\vec{n})+\alpha\Bigr]\Bigr\}
  \end{split}
  \label{eq:Txn_2d_01}
\end{equation}

avec $\theta$ l'angle entre la normale (sortante de la paroi) et la direction échantillonnée sur le cercle centré en $\vec{x_{N}}$. Un angle limite $\theta_{lim}=acos(\delta_{p}/\delta_{ref})$ apparaît. Dans l'intégrale précédente, le second Heaviside $\mathcal{H}\bigl(\delta_{ref}cos(\theta)>\delta_{p}\bigr)$ correspond à tous les angles entre 0 et $\theta_{lim}$; le premier Heaviside $\mathcal{H}\bigl(\delta_{ref}cos(\theta)<\delta_{p}\bigr)$ correspond aux angles entre $\theta_{lim}$ et $\pi/2$; en utilisant l'expression du profil quadratique de température, la relation \ref{eq:Txn_2d_01} devient donc:

\begin{equation}
  \begin{split}
    T(x_{N})=\frac{2}{\pi}\int_{0}^{\theta_{lim}}&-\frac{p}{2\lambda}\bigl(x_{N}^{2}+\delta_{p}^{2}\bigr)+ax_{N}+b+\alpha d\theta\\
    &+\frac{2}{\pi}\int_{\theta_{lim}}^{\pi/2} -\frac{p}{2\lambda}\bigl(x_{N}^{2}+\delta_{ref}^{2}cos^{2}(\theta)\bigr)+ax_{N}+b+\frac{p}{4\lambda}\delta_{ref}^{2} d\theta
  \end{split}
  \label{eq:Txn_2d_02}
\end{equation}

\begin{equation}
  \begin{split}
    T(x_{N})&=\frac{2}{\pi}\Bigl[-\frac{p}{2\lambda}\bigl(x_{N}^{2}+\delta_{p}^{2}\bigr)+ax_{N}+b+\alpha\Bigr]\theta_{lim}\\
    &+\frac{2}{\pi}\Bigl[-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b+\frac{p}{4\lambda}\delta_{ref}^{2}\Bigr]\Bigl(\frac{\pi}{2}-\theta_{lim}\Bigr)-\frac{p}{\pi\lambda}\delta_{ref}^{2}\int_{\theta_{lim}}^{\pi/2} cos^{2}(\theta)d\theta
  \end{split}
  \label{eq:Txn_2d_03}
\end{equation}

L'intégrale de $cos^{2}(\theta)$ entre $\theta_{lim}$ et $\pi/2$ peut s'écrire en fonction de $r=cos(\theta_{lim})=\frac{\delta_{p}}{\delta_{ref}}$:

\begin{equation}
  \int_{\theta_{lim}}^{\pi/2} cos^{2}(\theta)d\theta=\frac{1}{2}\Bigl[-r\sqrt{1-r^{2}}+\frac{\pi}{2}-\theta_{lim}\Bigr]
\end{equation}

On obtient:
\begin{equation}
  T(x_{N})=-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b+\frac{2\alpha\theta_{lim}}{\pi}+\frac{\delta_{ref}^{2}}{2\pi\lambda}r\sqrt{1-r^{2}}-\frac{p}{\pi\lambda}\delta_{p}^{2}\theta_{lim}
  \label{eq:Txn_2d_04}
\end{equation}

par égalité avec $T(x_{N})=-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b$, on obtient donc
\noindent\begin{minipage}{\linewidth}
$\frac{2\alpha\theta_{lim}}{\pi}+\frac{\delta_{ref}^{2}}{2\pi\lambda}r\sqrt{1-r^{2}}-\frac{p}{\pi\lambda}\delta_{p}^{2}\theta_{lim}=0$
\end{minipage}
d'où la valeur de $\alpha$ en 2D:
\begin{equation}
  \alpha(D=2)=-\frac{p}{4\lambda}\delta_{ref}^{2}\Bigl[\frac{r\sqrt{1-r^{2}}}{acos(r)}-2r^{2}\Bigr]
  \label{eq:alpha_2D}
\end{equation}



%-----------------------------------------------------------------------------------------------
\subsection{En 3D}

L'intégrale permetttant d'obtenir $T(\vec{x_{N}})$ doit cette fois-ci être réalisée en 3D, en tenant compte de l'angle solide élémentaire $d\vec{u}=sin(\theta) d\theta d\phi$; on retient:
\begin{equation}
  \begin{split}
    T(\vec{x_{N}})=\int_{0}^{\pi/2} sin(\theta)d\theta &d\theta \Bigl\{\mathcal{H}\bigl(\delta_{ref}cos(\theta)<\delta_{p}\bigr)\Bigl[\frac{1}{2}T(\vec{x_{N}}+\delta_{ref}cos(\theta)\vec{n})+\frac{1}{2}T(\vec{x_{N}}-\delta_{ref}cos(\theta)\vec{n})+\frac{p}{6}\delta_{ref}^{2}\Bigr]\\
    &+\mathcal{H}\bigl(\delta_{ref}cos(\theta)>\delta_{p}\bigr)\Bigl[\frac{1}{2}T(\vec{x_{N}}+\delta_{p}\vec{n})+\frac{1}{2}T(\vec{x_{N}}-\delta_{p}\vec{n})+\alpha\Bigr]\Bigr\}
  \end{split}
  \label{eq:Txn_3d_01}
\end{equation}

en utilisant le profil quadratique de température, on obtient:
\begin{equation}
  \begin{split}
    T(x_{N})&=\Bigl(-\frac{p}{2\lambda}\bigl(x_{N}^{2}+\delta_{p}^{2}\bigr)+ax_{N}+b\Bigr)\int_{0}^{\theta_{lim}} sin(\theta)d\theta\\
    &+\Bigl(-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b+\frac{p}{6\lambda}\delta_{ref}^{2}\Bigr)\int_{\theta_{lim}}^{\pi/2} sin(\theta)d\theta\\
      &-\frac{p}{2\lambda}\delta_{ref}^{2}\int_{\theta_{lim}}^{\pi/2} sin(\theta)cos^{2}(\theta)d\theta
  \end{split}
  \label{eq:Txn_3d_02}
\end{equation}

En utilisant:
\begin{equation}
  \begin{dcases}
    \int_{0}^{\theta_{lim}} sin(\theta)d\theta=1-r\\
    \int_{\theta_{lim}}^{\pi/2} sin(\theta)d\theta=r\\
    \int_{\theta_{lim}}^{\pi/2} sin(\theta)cos^{2}(\theta)d\theta=\frac{r^{3}}{3}
  \end{dcases}
\end{equation}

on obtient:
\begin{equation}
    T(x_{N})=-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b-\frac{p}{2\lambda}\delta_{p}^{2}\bigl(1-r\bigr)+\alpha\bigl(1-r\bigr)+\frac{p}{6\lambda}\delta_{ref}^{2}r\bigl(1-r^{2}\bigr)
  \label{eq:Txn_3d_03}
\end{equation}
par égalité avec $T(x_{N})=-\frac{p}{2\lambda}x_{N}^{2}+ax_{N}+b$, on a: 
\noindent\begin{minipage}{\linewidth}
$-\frac{p}{2\lambda}\delta_{p}^{2}\bigl(1-r\bigr)+\alpha\bigl(1-r\bigr)+\frac{p}{6\lambda}\delta_{ref}^{2}r\bigl(1-r^{2}\bigr)=0$
\end{minipage}

D'où la valeur de $\alpha$ en 3D:
\begin{equation}
  \alpha(D=3)=\frac{p}{6\lambda}\delta_{ref}^{2} r (2r-1)
  \label{eq:alpha_3D}
\end{equation}



%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
\section{Départ de la marche depuis une interface}

%-----------------------------------------------------------------------------------------------
\subsection{Depuis une interface entre deux solides}
\begin{figure}[!htbp]
\hspace*{-1.5cm}
\centering
 \mbox{
     \subfigure[]{\epsfig{figure=figures/walks_start_without_thermal_resistance.png,width=0.5\textwidth}}\quad
     \subfigure[]{\epsfig{figure=figures/walks_start_with_thermal_resistance,width=0.55\textwidth}}}
  \caption[]{Démarrage d'une marche conductive à partir d'une interface entre deux solides de conductivités $\lambda_{g}$ et $\lambda_{d}$ (a) en l'absence de résistance thermique et (b) en présence d'une résistance thermique à la jonction.}
\label{fig:walks_start_between_solids}
\end{figure}

Le raisonnement est conduit sur la base de la configuration représentée par les figures \ref{fig:walks_start_between_solids}: deux solides semi-infinis (de conductivité $\lambda_{g}$ et terme source $p_{g}$ à gauche, de conductivité $\lambda_{d}$ et terme source $p_{d}$ à droite) sont séparés par une interface plane. Le problème est maintenant d'exprimer la température de l'interface (dans le cas où il n'y a pas de résistance thermique à l'interface) ou les températures des deux côtés de l'interface (dans le cas où une résistance thermique doit être prise en compte) en fonction de $T_{g}$ situé à une distance $\delta_{g}$ de l'interface du côté gauche, et de $T_{d}$ situé à une distance $\delta_{d}$ de l'interface du côté droit. Les coefficients de pondération de ces deux températures doivent correspondre à des probabilités (i.e. leurs valeurs doivent être comprises entre 0 et 1, et leur somme doit être égale à 1) de façon à ce que l'expression des températures d'interface puisse être lue comme un algorithme de Monte-Carlo. Un terme fixe va se rajouter à cette somme: ce sera la valeur de l'accumulateur correspondant à la température d'interface à traiter.

Les profils de température dans les solides sont quadratiques:

\begin{equation}
  \begin{dcases}
    T(x)=-\frac{p_{g}}{2\lambda_{g}}x^{2}+a_{g}x+b\text{~pour le côté gauche}\\
    T(x)=-\frac{p_{d}}{2\lambda_{d}}x^{2}+a_{d}x+b\text{~pour le côté droit}
  \end{dcases}
  \label{eq:T2solids_00}
\end{equation}



%-----------------------------------------------------
\subsubsection{Sans résistance thermique à l'interface}

Dans le cas où il n'y a pas de résistance thermique à l'interface, il n'y a qu'une seule température $T_{p}$ à prendre en compte à la jonction (cf. figure \ref{fig:walks_start_between_solids}-a):

Si on choisit l'origine du repère à l'interface, on a donc:
\begin{equation}
  \begin{dcases}
    T(x)=-\frac{p_{g}}{2\lambda_{g}}x^{2}+a_{g}x+T_{p}\text{ pour }x<0\\
    T(x)=-\frac{p_{d}}{2\lambda_{d}}x^{2}+a_{d}x+T_{p}\text{ pour }x>0
  \end{dcases}
  \label{eq:T2solids_01}
\end{equation}

La densité surfacique de flux net à l'interface $\phi$ s'écrit:

\begin{equation}
  \phi=
  \begin{dcases}
    -\lambda_{g}\frac{dT}{dx}_{|x\rightarrow0^{-}}=-\lambda_{g}a_{g}\\
    -\lambda_{d}\frac{dT}{dx}_{|x\rightarrow0^{+}}=-\lambda_{d}a_{d}
  \end{dcases}
\end{equation}

d'où:
\begin{equation}
  \lambda_{g}a_{g}=\lambda_{d}a_{d}
  \label{eq:alambda}
\end{equation}

Les températures $T_{g}$ et $T_{d}$ peuvent être exprimées à partir de \ref{eq:T2solids_01} (respectivement pour $x=-\delta_{g}$ et $x=\delta_{d}$):
\begin{equation}
  \begin{dcases}
    T_{g}=-\frac{p_{g}}{2\lambda_{g}}\delta_{g}^{2}-a_{g}\delta_{g}+T_{p}\\
    T_{d}=-\frac{p_{d}}{2\lambda_{d}}\delta_{d}^{2}+a_{d}\delta_{d}+T_{p}
  \end{dcases}
  \label{eq:TgTd_01}
\end{equation}

En utilisant \ref{eq:alambda}, on obtient l'expression de $T_{p}$ en fonction de $T_{g}$ et $T_{d}$:
\begin{equation}
  T_{p}=p_{1}T_{g}+(1-p_{1})T_{d}+\alpha_{p}
  \label{eq:Tp}
\end{equation}

avec:
\begin{equation}
  \begin{dcases}
    p_{1}=\frac{\frac{\lambda_{g}}{\delta_{g}}}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}} \\
    \alpha_{p}=\frac{1}{2}\frac{p_{g}\delta_{g}+p_{d}\delta_{d}}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}}
  \end{dcases}
\end{equation}


%-----------------------------------------------------
\subsubsection{Avec résistance thermique à l'interface}

Dans le cas où une résistance thermique doit être prise en compte à l'interface, deux température $T_{p}^{-}$ et $T_{p}^{+}$ permettent de modéliser la jonction (cf. figure \ref{fig:walks_start_between_solids}-b):

Si on choisit l'origine du repère à l'interface, on a donc:
\begin{equation}
  \begin{dcases}
    T(x)=-\frac{p_{g}}{2\lambda_{g}}x^{2}+a_{g}x+T_{p}^{-}\text{ pour }x<0\\
    T(x)=-\frac{p_{d}}{2\lambda_{d}}x^{2}+a_{d}x+T_{p}^{+}\text{ pour }x>0
  \end{dcases}
  \label{eq:T2solids_02}
\end{equation}

La dérivation de ces expressions permet d'obtenir la relation \ref{eq:alambda}; mais dans le cas présent, on connaît la valeur de la densité surfacique de flux net à l'interface:
\begin{equation}
  \phi=\frac{T^{+}-T^{-}}{R}
\end{equation}

Ce qui permet d'obtenir les valeurs des coefficients $a_{g}$ et $a_{d}$:
\begin{equation}
  \begin{dcases}
    a_{g}=\frac{T^{+}-T^{-}}{R\lambda_{g}}\\
    a_{d}=\frac{T^{+}-T^{-}}{R\lambda_{d}}
  \end{dcases}
\end{equation}

Les expressions des températures $T_{g}$ et $T_{d}$ (respectivement pour $x=-\delta_{g}$ et $x=\delta_{d}$) sont donc:
\begin{equation}
  \begin{dcases}
    T_{g}=-\frac{p_{g}}{2\lambda_{g}}\delta_{g}^{2}-\frac{T^{+}-T^{-}}{R\lambda_{g}}\delta_{g}+T_{p}^{-}\\
    T_{d}=-\frac{p_{d}}{2\lambda_{d}}\delta_{d}^{2}+\frac{T^{+}-T^{-}}{R\lambda_{d}}\delta_{d}+T_{p}^{+}
  \end{dcases}
  \label{eq:TgTd_02}
\end{equation}

Ce qui constitue un système de deux équations dont les deux inconnues sont $T_{p}^{-}$ et $T_{p}^{+}$; on obtient:
\begin{equation}
  \begin{dcases}
    T_{g}^{-}=p_{1}T_{g}+(1-p_{1})T_{d}+\alpha_{p^{-}}\\
    T_{d}^{+}=p_{2}T_{g}+(1-p_{2})T_{d}+\alpha_{p^{+}}
  \end{dcases}
\end{equation}

avec:
\begin{equation}
  \begin{dcases}
    p_{1}=\frac{\frac{\lambda_{g}}{\delta_{g}}\bigl(1+\frac{R\lambda_{d}}{\delta_{d}}\bigr)}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}+R\frac{\lambda_{g}}{\delta_{g}}\frac{\lambda_{d}}{\delta_{d}}}\\
    p_{2}=\frac{\frac{\lambda_{g}}{\delta_{g}}}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}+R\frac{\lambda_{g}}{\delta_{g}}\frac{\lambda_{d}}{\delta_{d}}}\\
    \alpha_{p^{-}}=\frac{1}{2}\frac{p_{g}\delta_{g}\bigl(1+\frac{R\lambda_{d}}{\delta_{d}}\bigr)+p_{d}\delta_{d}}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}+R\frac{\lambda_{g}}{\delta_{g}}\frac{\lambda_{d}}{\delta_{d}}}\\
    \alpha_{p^{+}}=\frac{1}{2}\frac{p_{g}\delta_{g}+p_{d}\delta_{d}\bigl(1+\frac{R\lambda_{g}}{\delta_{g}}\bigr)}{\frac{\lambda_{g}}{\delta_{g}}+\frac{\lambda_{d}}{\delta_{d}}+R\frac{\lambda_{g}}{\delta_{g}}\frac{\lambda_{d}}{\delta_{d}}}
  \end{dcases}
\end{equation}


%-----------------------------------------------------------------------------------------------
\subsection{Depuis une interface entre un solide et un fluide}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{./figures/walks_start_with_fluid.png}
\caption[]{Démarrage d'une marche conductive à partir d'une interface où un solide de conductivité $\lambda$ est au contact d'un fluide semi-transparent: sont pris en charge l'échange convectif via le coefficient d'échange $h_{c}$ avec le fluide de température $T_{F}$, l'échange radiatif via le coefficient d'échange radiatif linéarisé $h_{r}$ avec une température radiative $T_{R}$, et une densité surfacique de flux net imposé à l'interface $\phi_{net}$.}
\label{fig:walks_start_with_fluid}
\end{figure}

Le raisonnement est toujours conduit sur la base d'un solide semi-infini muni d'un terme source, présentant une interface plane avec, cette fois-ci, un fluide semi-transparent (voir figure \ref{fig:walks_start_with_fluid}); un échange convectif avec le fluide est pris en compte, ainsi qu'un échange radiatif avec une température radiative isotrope. Une densité surfacique de flux net est également imposé à l'interface. Le profil de température dans le solide est toujours quadratique; en choisissant l'origine du repère à l'interface, ce profil devient:

\begin{equation}
  T(x)=-\frac{p}{2\lambda}x^{2}+ax+T_{p}
\end{equation}

Exprimé en $x=-\delta_{i}$, on obtient la température $T_{\delta}$:
\begin{equation}
  T_{\delta}=-\frac{p}{2\lambda}\delta_{i}^{2}-a\delta_{i}+T_{p}
  \label{eq:Tdelta}
\end{equation}

Par ailleurs, la densité surfacique de flux net à l'interface est:
\begin{equation}
  \phi=-\lambda\frac{dT}{dx}_{x\rightarrow 0^{-}}=h_{c}\bigl(T_{p}-T_{F}\bigr)+h_{r}\bigl(T_{p}-T_{R}\bigr)-\phi_{net}
\end{equation}

Etant donné que $\frac{dT}{dx}_{x\rightarrow 0^{-}}=a$, on obtient:
\begin{equation}
  a=-\frac{1}{\lambda}\Bigl[h_{c}\bigl(T_{p}-T_{F}\bigr)+h_{r}\bigl(T_{p}-T_{R}\bigr)-\phi_{net}\Bigr]
\end{equation}

Par injection dans \ref{eq:Tdelta}, on a:
\begin{equation}
  T_{p}=p_{1}T_{\delta}+p_{2}T_{F}+p_{3}T_{R}+\alpha_{p}
\end{equation}

avec:
\begin{equation}
  \begin{dcases}
    p_{1}=\frac{1}{1+\frac{h_{c}\delta}{\lambda}+\frac{h_{r}\delta}{\lambda}}\\
    p_{2}=\frac{\frac{h_{c}\delta}{\lambda}}{1+\frac{h_{c}\delta}{\lambda}+\frac{h_{r}\delta}{\lambda}}\\
    p_{3}=\frac{\frac{h_{r}\delta}{\lambda}}{1+\frac{h_{c}\delta}{\lambda}+\frac{h_{r}\delta}{\lambda}}\\
    \alpha_{p}=\frac{1}{2}\frac{\frac{p\delta^{2}}{\lambda}+2\frac{\delta\Phi_{net}}{\lambda}}{1+\frac{h_{c}\delta}{\lambda}+\frac{h_{r}\delta}{\lambda}}
  \end{dcases}
\end{equation}









%\bibliographystyle{unsrt}
%\bibliography{biblio}

\end{document}
