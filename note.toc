\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Arriv\IeC {\'e}e de la marche \IeC {\`a} une interface}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}En 2D}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}En 3D}{7}{subsection.2.2}
\contentsline {section}{\numberline {3}D\IeC {\'e}part de la marche depuis une interface}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}Depuis une interface entre deux solides}{8}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Sans r\IeC {\'e}sistance thermique \IeC {\`a} l'interface}{9}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Avec r\IeC {\'e}sistance thermique \IeC {\`a} l'interface}{9}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Depuis une interface entre un solide et un fluide}{10}{subsection.3.2}
